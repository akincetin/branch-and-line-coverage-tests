package com.coverage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class gcdTest6 {
    @Test
    @DisplayName("com.coverage.gcd(2,2) test case")
    void test1 (){
        assertEquals(2, gcd.gcd(2,2));
    }

    @Test
    @DisplayName("com.coverage.gcd(6,18) test case")
    void test2 (){
        assertEquals(6, gcd.gcd(6,18));
    }

}