package com.coverage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class gcdTest7 {

    @Test
    @DisplayName("com.coverage.gcd(6,18) test case")
    void test2 (){
        assertEquals(6, gcd.gcd(6,18));
    }

    @Test
    @DisplayName("com.coverage.gcd(12,18) test case")
    void test3 (){
        assertEquals(6, gcd.gcd(12,18));
    }


}