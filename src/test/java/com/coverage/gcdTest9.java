package com.coverage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class gcdTest9 {
    @Test
    @DisplayName("com.coverage.gcd(12,18) test case")
    void test3 (){
        assertEquals(6, gcd.gcd(12,18));
    }
    @Test
    @DisplayName("com.coverage.gcd(-1,-1) test case")
    void test4 (){
        assertEquals(-1, gcd.gcd(-1,-1));
    }

    @Test
    @DisplayName("com.coverage.gcd(0,-1) test case")
    void test5 (){
        assertEquals(-1, gcd.gcd(0,-1));
    }

}