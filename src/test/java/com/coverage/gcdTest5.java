package com.coverage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class gcdTest5 {

    @Test
    @DisplayName("com.coverage.gcd(0,-1) test case")
    void test5 (){
        assertEquals(-1, gcd.gcd(0,-1));
    }


}