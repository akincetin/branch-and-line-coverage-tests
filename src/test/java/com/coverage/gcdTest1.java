package com.coverage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class gcdTest1 {

    @Test
    @DisplayName("com.coverage.gcd(2,2) test case")
    void test1 (){
        assertEquals(2, gcd.gcd(2,2));
    }

}