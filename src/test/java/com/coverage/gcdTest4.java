package com.coverage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class gcdTest4 {
    @Test
    @DisplayName("com.coverage.gcd(-1,-1) test case")
    void test4 (){
        assertEquals(-1, gcd.gcd(-1,-1));
    }

}